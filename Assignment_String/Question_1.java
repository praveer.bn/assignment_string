package week4.Assignment_String;


import java.util.Arrays;

class Conversion{
    public void arr() {
        String[] array = {"ironman", "captain america", "drstrange", "hulk", "spiderman", "thanos"};
        for (int i = 0; i < array.length; i++) {
            char chararray[] = array[i].toCharArray();
//            for (char c:chararray) {
//                System.out.println(c);
//            }
            System.out.println(Arrays.toString(chararray));
        }
    }
}

class Unicode{
     String string1=new String("   No matter how devastated you may be by your" +
            " own weakness or uselessness set your heart ablaze   ");
    public void trim(){
        System.out.println("after trimming:"+string1.trim());
        System.out.println("the unicode at index 5 is "+string1.codePointAt(5));

    }
}
class RepetativeString{
    public  void remove(){
        String string1=new String("No matter how devastated you may be by your" +
                " own weakness or uselessness set your heart ablaze ");
        String newstring=" ";
        for (int i = 0; i <string1.length() ; i++) {
            char ch=string1.charAt(i);
            if(newstring.indexOf(ch)<0){
                newstring +=ch;
            }

        }
        System.out.println(newstring);
    }
}
class Question_2{
    public void vowels(){
        String string=new String("No matter how devastated you may be by your" +
                " own weakness or uselessness set your heart ablaze ");
         String  s1=string.toLowerCase();
         int countvowels=0;
         int countconsonant=0;
        for (int i = 0; i < s1.length(); i++) {
            if(s1.charAt(i) =='a'||s1.charAt(i)=='e'||s1.charAt(i)=='i'||s1.charAt(i)=='o'||s1.charAt(i)=='u'){
               countvowels++;
            } else {
                countconsonant++;
            }

        }
        System.out.println("number of vowels in String is "+countvowels);
        System.out.println("number of vowels in String is "+countconsonant);
    }
}

public class Question_1 {
    public static void main(String[] args) {
//        Question 6
        Conversion conversion=new Conversion();
        conversion.arr();
        System.out.println("*****************************************");
//        question 4
        Unicode unicode=new Unicode();
        unicode.trim();
        System.out.println("**********************************************************************");
//        question 3
        RepetativeString repetativeString=new RepetativeString();
        repetativeString.remove();
        System.out.println("**********************************************************************");
//        Question 2
        Question_2 question_2=new Question_2();
        question_2.vowels();
        System.out.println("**********************************************************************");
//        Question 1
        String[] array = {"ironman", "captain america", "drstrange", "hulk", "spiderman", "thanos"};
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i].length() > array[j].length()) {
                    String temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));

    }
}
