package week4.Assignment_String;
class Empolyee{
    int emp_id;
    int salary;
    String name;

    public Empolyee() {
    }

    public Empolyee(int emp_id, int salary, String name) {
        this.emp_id = emp_id;
        this.salary = salary;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Empolyee{" +
                "emp_id=" + emp_id +
                ", salary=" + salary +
                ", name='" + name + '\'' +
                '}';
    }
}
public class Question_9 {
    public static void main(String[] args) {

        Empolyee empolyee = new Empolyee();
        System.out.println("before initialization");
        System.out.println(empolyee);

        Empolyee empolyee1 = new Empolyee(111,30000,"Praveer");
        System.out.println("after initialization");
        System.out.println(empolyee1);

    }
}
