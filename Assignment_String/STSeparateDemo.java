package week4.Assignment_String;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class STSeparateDemo {
    public static void main(String[] args) {
        multiDelimeter1("3+(20%2)*(20/2)");
        multiDelimeter2("3+(20%2)*(20/2)");
    }


    public static void multiDelimeter1(String String){
        StringTokenizer stringTokennizer=new StringTokenizer(String,", .   ? ( ) * / + %",true);
        System.out.println("token with delimeter ");
        ArrayList<String> al=new ArrayList<>();
        while (stringTokennizer.hasMoreTokens()){
            String s=stringTokennizer.nextToken();
            al.add(s);
            System.out.println(s);
        }
        System.out.println("operator:");
        for (String s:al) {
            if (s.equals("+")||s.equals("/")||s.equals("%")||s.equals("*")||s.equals("(")||s.equals(")"))
                System.out.println(s);
        }
        System.out.println("operand:");
        for (String s:al) {
            if (s.equals("+")||s.equals("/")||s.equals("%")||s.equals("*")||s.equals("(")||s.equals(")")) {
            }else{ System.out.println(s);}
        }
    }
    public static void multiDelimeter2(String String){
        StringTokenizer stringTokennizer=new StringTokenizer(String,", .   ? ( ) * / + %");
        System.out.println("token without delimeter ");
        while (stringTokennizer.hasMoreTokens()){
            System.out.println( stringTokennizer.nextToken());
        }
    }
}

