package week4.Assignment_String;


import java.util.Arrays;
import java.util.Scanner;

public class Question_6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter the element in array");
        String[] array = new String[5];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextLine();
        }
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        System.out.println("sorted array:");
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i].length() > array[j].length()) {
                    String temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
        for (int i = 0; i < array.length; i++) {
            char[] chararray = array[i].toCharArray();
            for (int j = 0; j < chararray.length ; j++) {
                if (chararray[j] == 'a' || chararray[j] == 'e' || chararray[j] == 'i' || chararray[j] == 'o'
                        || chararray[j] == 'u') {
                    chararray[j]++;
                }
            }
                String result = " ";
                for (int k = 0; k < chararray.length; k++) {
                    result += chararray[k];
                }

                array[i] = result;
        }
        System.out.println("updated string");
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }

}

/*output
enter the element in array
praveer
ayshman
ashraff
aman
sanyami


praveer
ayshman
ashraff
aman
sanyami
sorted array:
[aman, ayshman, ashraff, praveer, sanyami]


updated string
 bmbn
 byshmbn
 bshrbff
 prbvffr
 sbnybmj

 */